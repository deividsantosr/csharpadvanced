﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_Delegate
{
    class Program
    {
        delegate int Calc(int a, int b);

        static void Main(string[] args)
        {
            //var sum = Sum(10, 5);
            //var sub = Subtraction(10, 5);

            Calc calcSum = new Calc(Sum);
            Calc calcSub = new Calc(Subtraction);

            var sum = calcSum(10, 5);
            var sub = calcSub(10, 5);


            Console.WriteLine(sum);
            Console.WriteLine(sub);

            Console.ReadKey();
        }

        public static int Sum(int a, int b)
        {
            return a + b;
        }

        public static int Subtraction(int a, int b)
        {
            return a - b;
        }
    }
}
