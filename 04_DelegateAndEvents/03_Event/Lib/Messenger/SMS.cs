﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _03_Event.Lib.Messenger
{
    class SMS
    {
        public void SendMessage(object sender, VideoEventArgs args)
        {
            Console.WriteLine("SMS sent to the video {0}", args.Video.Name);
        }
    }
}
