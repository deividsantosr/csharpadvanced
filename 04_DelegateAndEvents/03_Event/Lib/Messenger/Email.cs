﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _03_Event.Lib.Messenger
{
    class Email
    {
        public void SendMessage(object sender, VideoEventArgs video)
        {
            Console.WriteLine("Email sent to the video {0}", video.Video.Name);
        }
    }
}
