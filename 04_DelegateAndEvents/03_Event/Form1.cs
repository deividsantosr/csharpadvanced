﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using _03_Event.Lib;
using _03_Event.Lib.Messenger;

namespace _03_Event
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            btnVideoEncode.Click += EventClick;

            btnVideoEncode.Click += delegate (object sender, EventArgs e)
            {
                MessageBox.Show("Olá! Fui clicado!");
            };

            btnVideoEncode.Click += delegate
            {
                MessageBox.Show("Olá! Fui clicado novamente!");
            };
        }

        public void EventClick(object sender, EventArgs e)
        {
            Video video = new Video() { Name = "video.mp4" };

            VideoEncode videoEncode = new VideoEncode();
            videoEncode.Encoded += new SMS().SendMessage;
            videoEncode.Encoded += new Email().SendMessage;

            videoEncode.Encode(video);
        }
    }
}
