﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using _02_Delegate.Lib;

namespace _02_Delegate
{
    class Program
    {
        static void Main(string[] args)
        {
            Photo photo = new Photo() { Name = "photo.jpg", SizeX = 1920, SizeY = 1080 };
            Photo photo2 = new Photo() { Name = "product.jpg", SizeX = 1920, SizeY = 1080 };

            PhotoProcessor.filter = new PhotoFilter().ToColor;
            PhotoProcessor.Processor(photo);

            PhotoProcessor.filter = new PhotoFilter().GenerateThumbnail;
            PhotoProcessor.filter += new PhotoFilter().Resize;
            PhotoProcessor.Processor(photo2);

            Console.ReadKey();
        }
    }
}
