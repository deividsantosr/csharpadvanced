﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_Delegate.Lib
{
    class PhotoFilter
    {
        public void ToColor(Photo photo)
        {
            Console.WriteLine("PhotoFilter > ToColor: {0}", photo.Name);
        }

        public void GenerateThumbnail(Photo photo)
        {
            Console.WriteLine("PhotoFilter > GenerateThumbnail: {0}", photo.Name);
        }

        public void BlackAndWthite(Photo photo)
        {
            Console.WriteLine("PhotoFilter > BlackAndWhite: {0}", photo.Name);
        }

        public void Resize(Photo photo)
        {
            Console.WriteLine("PhotoFilter > Resize: {0}", photo.Name);
        }
    }
}
