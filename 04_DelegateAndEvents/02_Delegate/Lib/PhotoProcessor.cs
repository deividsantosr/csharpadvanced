﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_Delegate.Lib
{
    class PhotoProcessor
    {
        public delegate void PhotoFilterHandler(Photo photo);

        public static PhotoFilterHandler filter;

        public static void Processor(Photo photo)
        {
            filter(photo);

            //var filter = new PhotoFilter();
            //filter.ToColor(photo);
            //filter.GenerateThumbnail(photo);
            //filter.BlackAndWthite(photo);
            //filter.Resize(photo);
        }
    }
}