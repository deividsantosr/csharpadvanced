﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_VarAndDynamic
{
    class Program
    {
        static void Main(string[] args)
        {
            object t1 = "";
            object t2 = 123456;
            t2 = "";

            Console.WriteLine(t2);

            var v1 = "";
            var v2 = 123456;
            //v2 = "Not can be string";
            //var v3; //Should have a value

            Console.WriteLine(v2);

            //Run time exception
            dynamic d1 = new User() { Name = "Deivid Santos" };
            Console.WriteLine(d1.Name);
            //Console.WriteLine(d1.Senha);

            d1 = "Now d1 is String";

            Console.WriteLine(d1);

            Console.ReadKey();
        }

        public class User
        {
            public User()
            {
            }

            public string Name { get; set; }
        }
    }
}
