﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System
{
    static class StringExtension
    {
        public static string FirstToUpper(this String str)
        {
            string first = str.Substring(0, 1).ToUpper();
            string fromSecond = str.Substring(1);

            return first + fromSecond;
        }

    }
}
