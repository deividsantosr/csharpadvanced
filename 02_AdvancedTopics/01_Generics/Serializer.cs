﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Web.Script.Serialization;

namespace _01_Generic
{
    class Serializer
    {
        public static void Serialize(object obj)
        {
            StreamWriter streamWriter = new StreamWriter(@"C:\Users\deivid.santos\Documents\CSharpAdvanced\" + obj.GetType().Name + "_Serialize.txt");

            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();

            streamWriter.Write(javaScriptSerializer.Serialize(obj));
            streamWriter.Close();
        }

        public static T Unserialize<T>()
        {
            StreamReader streamReader = new StreamReader(@"C:\Users\deivid.santos\Documents\CSharpAdvanced\" + typeof(T).Name + "_Serialize.txt");

            JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();

            T obj = (T)javaScriptSerializer.Deserialize(streamReader.ReadToEnd(), typeof(T));

            return obj;
        }
    }
}
