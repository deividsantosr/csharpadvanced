﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using _01_Generics.Models;
using _01_Generic;

namespace _01_Generics
{
    class Program
    {
        static void Main(string[] args)
        {
            Car car = new Car() { Brand = "Fiat", Model = "Uno" };
            Home home = new Home() { Address = "São Paulo", City = "São Paulo" };
            User user = new User() { Name = "Deivid Sntos", CPF = "426.008.548.45", Email = "deivid.santos@fitbank.com.br" };

            Serializer.Serialize(car);
            Serializer.Serialize(home);
            Serializer.Serialize(user);

            Car car2 = Serializer.Unserialize<Car>();
            Home home2 = Serializer.Unserialize<Home>();
            User user2 = Serializer.Unserialize<User>();

            Console.WriteLine("Carro2: {0}, {1}", car.Brand, car.Model);

            Console.WriteLine("Home2: {0}, {1}", home2.City, home2.Address);

            Console.WriteLine("User2: {0}, {1}, {2}", user2.Name, user2.CPF, user2.Email);

            Console.ReadKey();
        }
    }
}
