﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_Generics.Models
{
    class Home
    {
        public string City { get; set; }
        public string Address { get; set; }
    }
}
