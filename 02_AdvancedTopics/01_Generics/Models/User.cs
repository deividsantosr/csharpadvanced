﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_Generics.Models
{
    class User
    {
        public string Name { get; set; }
        public string CPF { get; set; }
        public string Email { get; set; }
    }
}
