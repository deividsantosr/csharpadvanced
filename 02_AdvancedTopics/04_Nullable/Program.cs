﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_Nullable
{
    class Program
    {
        static void Main(string[] args)
        {
            //Struct - Por valor
            //int i = null;

            // Class - Por referência
            object o = null;

            Nullable<int> Age = null;
            int? Age2 = null; //Simple code
        }
    }
}
