﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_Attribute
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Field | AttributeTargets.Property | AttributeTargets.Method)]
    class MyAttribute : Attribute
    {
        public string Name { get; set; }
        public string Description { get; set; }

        public MyAttribute(string name)
        {
            Name = name;
        }
    }
}
