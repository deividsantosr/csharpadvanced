﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

namespace _03_Task
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Task> list = new List<Task>
            {
                Task.Factory.StartNew(Repetition),
                Task.Factory.StartNew(Repetition),
                Task.Factory.StartNew(Repetition),
                Task.Factory.StartNew(Repetition),
                Task.Factory.StartNew(Repetition)
            };

            Task.WaitAll(list.ToArray());
            //Task.WhenAll(list.ToArray());  



            List<String> addressList = new List<String>() {
                "http://www.google.com.br",
                "http://www.microsoft.com.br",
                "http://www.ibm.com.br",
            };

            var webClientAddress = from itemAddress in addressList select DownloadPage(itemAddress);

            Task.WaitAll(webClientAddress.ToArray());

            Console.WriteLine("Finish program");

            Console.ReadKey();
        }

        public static async Task DownloadPage(string address)
        {
            WebClient webClient = new WebClient();
            await webClient.DownloadStringTaskAsync(address);
            Console.WriteLine("Download successfully " + address);
        }

        public static void Repetition()
        {
            for (int i = 0; i <= 2; i++)
            {
                Console.WriteLine("Task Id: {0}, Value: {1}", Task.CurrentId, i);
            }
        }
    }
}
