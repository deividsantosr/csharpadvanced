﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;

namespace _01_Task
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private async void btnDownload_Click(object sender, EventArgs e)
        {
            string website = txtWebsite.Text;

            WebClient webClient = new WebClient();

            //txtResult.Text = webClient.DownloadString(website);

            string html = await webClient.DownloadStringTaskAsync(website);

            txtResult.Text = html;
        }
    }
}
