﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_Task
{
    class Program
    {
        static void Main(string[] args)
        {
            Task.Run(() => Repetition());
            Task.Run(() => Repetition());

            Task.Factory.StartNew(Repetition);

            Console.ReadKey();
        }

        public static void Repetition()
        {
            for (int i = 0; i <= 1000; i++)
            {
                Console.WriteLine("Task Id: {0}, Value: {1}", Task.CurrentId, i);
            }
        }
    }
}
