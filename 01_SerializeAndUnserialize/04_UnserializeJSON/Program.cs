﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using _00_Lib;
using System.IO;

namespace _04_UnserializeJSON
{
    class Program
    {
        static void Main(string[] args)
        {
            StreamReader stream = new StreamReader(@"C:\Users\deivid.santos\Documents\CSharpAdvanced\jsonSerializer.json");

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            User user = (User)serializer.Deserialize(stream.ReadToEnd(), typeof(User));

            Console.Write("Usuário(Nome): {0}, CPF: {1} e Email: {2}", user.Name, user.CPF, user.Email);
            Console.ReadKey();
        }
    }
}
