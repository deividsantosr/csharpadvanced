﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using _00_Lib;
using System.IO;
using System.Xml.Serialization;

namespace _02_UnserializeXML
{
    class Program
    {
        static void Main(string[] args)
        {
            StreamReader streamReader = new StreamReader(@"C:\Users\deivid.santos\Documents\CSharpAdvanced\xmlSerializer.xml");

            XmlSerializer xmlSerializer = new XmlSerializer(typeof(User));
            User user = (User)xmlSerializer.Deserialize(streamReader);

            Console.WriteLine("Usuário(Nome): {0}, CPF: {1} e Email: {2}", user.Name, user.CPF, user.Email);
            Console.ReadKey(); //Do not close the console
        }
    }
}
