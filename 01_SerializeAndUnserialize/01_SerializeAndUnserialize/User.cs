﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _00_Lib
{
    public class User
    {
        public string Name { get; set; }
        public string CPF { get; set; }
        public string Email { get; set; }
    }
}
