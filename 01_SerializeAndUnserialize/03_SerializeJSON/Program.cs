﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using _00_Lib;
using System.IO;

namespace _03_SerializeJSON
{
    class Program
    {
        static void Main(string[] args)
        {
            User user = new User() { Name = "Deivid Santos", Email = "deividnetwork@gmail.com", CPF = "426.008.548.45" };

            JavaScriptSerializer serializer = new JavaScriptSerializer();

            StreamWriter streamWriter = new StreamWriter(@"C:\Users\deivid.santos\Documents\CSharpAdvanced\jsonSerializer.json");
            streamWriter.WriteLine(serializer.Serialize(user));
            streamWriter.Close();
        }
    }
}
