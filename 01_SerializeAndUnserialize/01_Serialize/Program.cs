﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using _00_Lib;
using System.Xml.Serialization;

namespace _01_SerializeXML
{
    class Program
    {
        static void Main(string[] args)
        {
            User user = new User() { Name = "Deivid Santos", Email = "deivid.santos@fitbank.com.br", CPF = "426.008.548.45" };

            XmlSerializer xmlSerializer = new XmlSerializer(typeof(User));
            //XmlSerializer xmlSerializer = new XmlSerializer(user.GetType());

            StreamWriter stream = new StreamWriter(@"C:\Users\deivid.santos\Documents\CSharpAdvanced\xmlSerializer.xml");

            xmlSerializer.Serialize(stream, user);

        }
    }
}
