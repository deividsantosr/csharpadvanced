﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_LINQ
{
    class Program
    {
        static void Main(string[] args)
        {
            List<User> list = new List<User>();
            list.Add(new User() { Name = "User 1", Email = "user.1@gmail.com" });
            list.Add(new User() { Name = "User 5", Email = "user.5@otmail.com" });
            list.Add(new User() { Name = "User 3", Email = "user.3@gmail.com" });
            list.Add(new User() { Name = "User 4", Email = "user.4@outlook.com" });
            list.Add(new User() { Name = "User 2", Email = "user.2@gmail.com" });

            var filteredList = list.Where(a => a.Email.Contains("@gmail")).OrderBy(a => a.Name).Select(a => a);

            foreach (var user in filteredList)
            {
                Console.WriteLine("Nome: {0}, Email: {1}", user.Name, user.Email);
            }

            Console.ReadKey();
        }
    }
}
