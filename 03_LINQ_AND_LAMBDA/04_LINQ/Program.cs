﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_LINQ
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] list = { 5, 5, 5, 5, 15, 15, 20, 35, 30, 30, 10, 10, 10 };

            //var filteredList = list.Distinct().OrderBy(n => n).Select(n => n);
            var filteredList = list.OrderBy(n => n).GroupBy(n => n);

            foreach (var item in filteredList)
            {
                Console.WriteLine(item.Key);
            }

            Console.ReadKey();
        }
    }
}
