﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_LINQ
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] list = { 5, 10, 15, 30, 25, 20 };

            //(input) => expression 
            var filteredList = list.Where(n => n >= 15).OrderBy(n => n).Select(n => n);
            //var filteredList = from n in list where n >= 15 orderby n select n;

            foreach (var item in filteredList)
            {
                Console.WriteLine(item);
            }

            Console.ReadKey();
        }
    }
}
