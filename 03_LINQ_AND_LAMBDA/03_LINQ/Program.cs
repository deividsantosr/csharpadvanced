﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _03_LINQ
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Author> authorList = new List<Author>();
            List<Book> bookList = new List<Book>();

            authorList.Add(new Author() { Id = 1, Name = "Leonardo" });
            authorList.Add(new Author() { Id = 2, Name = "Maria" });
            authorList.Add(new Author() { Id = 3, Name = "Joshep" });

            bookList.Add(new Book() { Id = 1, AuthorId = 2, Title = "Amor amado" });
            bookList.Add(new Book() { Id = 2, AuthorId = 2, Title = "Bem amado" });
            bookList.Add(new Book() { Id = 3, AuthorId = 3, Title = "Um espião em Washigton" });
            bookList.Add(new Book() { Id = 4, AuthorId = 1, Title = "A vida na terra" });
            bookList.Add(new Book() { Id = 4, AuthorId = 1, Title = "A vida na terra" });

            var joinList = from book in bookList join author in authorList on book.AuthorId equals author.Id select new { book, author };

            foreach (var item in joinList)
            {
                Console.WriteLine("Livro: {0}, Autor: {1}", item.book.Title, item.author.Name);
            }

            Console.ReadKey();
        }
    }
}
