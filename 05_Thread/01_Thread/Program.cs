﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace _01_Thread
{
    class Program
    {
        static void Main(string[] args)
        {
            Thread t1 = new Thread(RepetitionThread);
            t1.Start();

            for (int i = 0; i <= 1000; i++)
            {
                Console.WriteLine("Main: {0}", i);
            }

            Console.ReadKey();
        }

        //IO - Screen, Network, Storage...
        public static void RepetitionThread()
        {
            for (int i = 0; i <= 1000; i++)
            {
                Console.WriteLine("Num: {0}", i);
            }
        }
    }
}
