﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace _02_Thread
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int i = 0; i <= 5; i++)
            {
                Console.WriteLine("Thread {0} initialized", i);
                Thread t1 = new Thread(RepetitionThread)
                {
                    IsBackground = true //Roda independente do fluxo prinicpal em background
                };
                t1.Start();
            }

            Console.WriteLine("Program completed");
            Console.ReadKey();
        }

        //IO - Screen, Network, Storage...
        public static void RepetitionThread()
        {
            for (int i = 0; i <= 1000; i++)
            {
                Console.WriteLine("Num: {0}", i);
            }
        }
    }
}
