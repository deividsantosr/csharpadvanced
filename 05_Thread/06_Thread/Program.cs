﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace _06_Thread
{
    class Program
    {
        public static ManualResetEvent manualReset;
        public static AutoResetEvent autoReset;

        static void Main(string[] args)
        {
            Console.WriteLine("Start Main Thread");

            manualReset = new ManualResetEvent(false);
            autoReset = new AutoResetEvent(false);

            Thread t1 = new Thread(Execute01);
            //t1.Start();

            Thread t2 = new Thread(Execute02);
            t2.Start();

            Thread.Sleep(3000);
            manualReset.Set();
            manualReset.Reset();

            autoReset.Set();

            Thread.Sleep(3000);
            autoReset.Set();

            Console.WriteLine("Finish Main Thread");

            Console.ReadKey();
        }

        public static void Execute01()
        {
            Console.WriteLine("01 Starting Execute01()");
            //Thread.Sleep(1000);
            manualReset.WaitOne();
            Console.WriteLine("02 Running Execute01()");
            Console.WriteLine("03 Running Execute01()");
            Console.WriteLine("04 Running Execute01()");
            manualReset.WaitOne();
            Console.WriteLine("05 Finishing Execute01()");
        }

        public static void Execute02()
        {
            Console.WriteLine("01 Starting Execute02()");
            autoReset.WaitOne();
            Console.WriteLine("02 Running Execute02()");
            Console.WriteLine("03 Running Execute02()");
            Console.WriteLine("04 Running Execute02()");
            autoReset.WaitOne();
            Console.WriteLine("05 Finishing Execute02()");
        }
    }
}
