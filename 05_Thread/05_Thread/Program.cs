﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace _05_Thread
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Initialize program");
            Thread.Sleep(2000);
            Console.WriteLine("Finish program");

            Thread t1 = new Thread(RepetitionThread);
            Thread t2 = new Thread(RepetitionThread);

            t1.Start();            
            t2.Start();
            t2.Join();

            Console.WriteLine("Generic text here");
            Console.WriteLine("Generic text here");
            Console.WriteLine("Generic text here");
            Console.WriteLine("Generic text here");
            Console.WriteLine("Generic text here");

            Console.ReadKey();
        }

        //IO - Screen, Network, Storage...''
        public static void RepetitionThread(object ThreadId)
        {
            for (int i = 0; i <= 1000; i++)
            {
                Console.WriteLine(" Id From System: {0}, Num: {1}", Thread.CurrentThread.ManagedThreadId, i);
            }
        }
    }


}
