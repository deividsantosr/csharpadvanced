﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace _04_Thread
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int i = 0; i <= 5; i++)
            {
                Console.WriteLine("Thread {0} initialized", i);
                Thread t1 = new Thread(RepetitionThread);
                t1.Start(i);
            }

            Console.WriteLine("Program completed");
            Console.ReadKey();
        }

        //IO - Screen, Network, Storage...''
        public static void RepetitionThread(object ThreadId)
        {
            for (int i = 0; i <= 1000; i++)
            {
                Console.WriteLine("Thread Id: {0}, Thread Id From System: {1}, Num: {2}", ThreadId, Thread.CurrentThread.ManagedThreadId, i);
            }
        }
    }
}
